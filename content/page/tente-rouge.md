# Tentes rouges

## Livres

* Anita Diamant - La tente rouge ou les filles de Jacob selon l'édition

## Références

* [Le mouvement à l'origine des TR aux States](http://www.birththeplay.org/red-tents/)
* [site de DDF](https://tentesrouges.fr/)

## Trame

* Accueil
* Rappel des règles de de prise de parole et de confidentialité sous la tente.
* Historique ou origine des TR
* Temps d'échange et de partage.
* Rite de cloture

# Tentes blanches

* https://oncletom.io/2019/12/14/pourquoi-groupe-hommes/
* https://oncletom.io/2019/10/23/groupe-hommes/